## Part 2 – Exploratory Data Analysis:

**Estimated time needed:** 45 minutes

In this Module you will download the dataset provided below and read it into a data frame. The goal of this exercise is to analyze the dataset and find out interesting insights from it.


### Software Used in this Assignment
You will be using IBM Watson Studio.

Although you can use any local application on your desktop to work on the problem,<ins>it is recommended that you use IBM Watson Studio</ins> as the lab instructions specifically refer to it.

### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://www.kaggle.com/uciml/restaurant-data-with-consumer-ratings under a Public Domain license. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset below.

Download the data file <a href="filename" target="_blank">**dataset.txt**</a>.

### Assignment Notebook

Download the notebook from here: <a href="filename" target="_blank">**EDA.ipynb**</a>.


