## Part 5 – Model Development and Validation:

**Estimated time needed:** 60 minutes

The goal of this exercise is to do Model Development to find the answer to the following question:

Can a predictive model be built for future prediction of the restaurant being liked or disliked by the customers?

Use your machine learning skills to build a predictive model to help a business function more efficiently.


### Software Used in this Assignment
You will be using IBM Watson Studio.

Although you can use any local application on your desktop to work on the problem,<ins>it is recommended that you use IBM Watson Studio</ins> as the lab instructions specifically refer to it.

### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://www.kaggle.com/uciml/restaurant-data-with-consumer-ratings under a Public Domain license. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset below.

Download the data file <a href="filename" target="_blank">**dataset.csv**</a>.

### Assignment Notebook

Download the notebook from here: <a href="filename" target="_blank">**Model.ipynb**</a>.

