## Part 3 – Data Analysis using SQL:

**Estimated time needed:** 45 minutes

In this Module you will perform the following tasks:

1. Download the dataset and become familiar with what it contains.
2. LOAD the datasets into a database
3. Utilize Python based Jupyter notebooks to compose and run SQL queries that retrieve specific results.

Once you have performed these tasks you will be tested in demonstrating working knowledge of SQL and correctness of queries and their result sets.


### Software Used in this Assignment
You will be using IBM Watson Studio & IBM Db2.

Although you can use any local application on your desktop to work on the problem,<ins>it is recommended that you use IBM Watson Studio</ins> as the lab instructions specifically refer to it.

### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://www.kaggle.com/uciml/restaurant-data-with-consumer-ratings under a Public Domain license. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset below.

Download the data file <a href="filename" target="_blank">**dataset.csv**</a>.

### Assignment Notebook

Download the notebook from here: <a href="filename" target="_blank">**SQL.ipynb**</a>.
