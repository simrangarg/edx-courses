## Part 1 – Data Loading & Cleaning:

**Estimated time needed:** 45 minutes

In this Module you will download the dataset provided below and load & understand the data and perform data cleaning to get the final cleaned data frame for further modules of the course.


### Software Used in this Assignment
You will be using IBM Watson Studio.

Although you can use any local application on your desktop to work on the problem,<ins>it is recommended that you use IBM Watson Studio</ins> as the lab instructions specifically refer to it.

### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://www.kaggle.com/uciml/restaurant-data-with-consumer-ratings under a Public Domain license. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset provided within each module, rather than the dataset from the original source.

Download the file <a href="filename.csv" target="_blank">**dataset.CSV**</a>.

### Assignment Notebook

Download the notebook from here: <a href="filename.csv" target="_blank">**Loading&Cleaning Data.ipynb**</a>.


