*If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:*

#### Step 1: For New Users (with no Watson service):

Go to the IBM Cloud Watson Studio page:

[Click here](https://cloud.ibm.com/catalog/services/watson-studio)

You will see the screen in the figure below. Click the option in the red box.

<img src="./images/New1.PNG" width="550"><br />

You will see the screen in the figure below. Enter **Email** and **Password** and the click **Next**.

<img src="./images/New2.PNG" width="550"><br />

Enter the verification code to verify email and click **Next**.

<img src="./images/New3.PNG" width="550"><br />

Enter your personal information and click **Next**.

<img src="./images/New4.PNG" width="550"><br />

Accept the terms and conditions and click on **Create account**.

You will see the screen in the figure below. Click on the **Create** button.

<img src="./images/New5.PNG" width="550"><br />

Now click **Get Started**.

<img src="./images/New6.PNG" width="550"><br />

Then click **Go to IBM Watson Studio**.

<img src="./images/New7.PNG" width="550"><br />

Then click **Continue**.

<img src="./images/New8.PNG" width="550"><br />

After creating the service continue with Step 2.

#### Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services**.

<img src="./images/DA0101EN_FA_Image7.png" width="550"><br />

When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

<img src="./images/DA0101EN_FA_Image8.png" width="550"><br />

Then click **Get Started**.

<img src="./images/New6.PNG" width="550"><br />

#### Step 3: Creating a Project

Now you have to Create a project.

Click on **Create a project**.

<img src="./images/New9.PNG" width="550"><br />

On the Create a project page, click **Create an empty project**.

<img src="./images/New10.PNG" width="550"><br />

Provide a **Project Name** and **Description**.

<img src="./images/New11.PNG" width="550"><br />

You must also create storage for the project.

Click **Add**

<img src="./images/New12.PNG" width="550"><br />

On the Cloud Object Storage page, click **Create**.

<img src="./images/New13.PNG" width="550"><br />

On the New project page, note that the storage has been added, click **Refresh** and then click **Create**.

<img src="./images/New14.PNG" width="550"><br />

After creating the project continue with Step 4.

#### Step 4: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project**.

<img src="./images/New15.PNG" width="550"><br />

In the list of asset types, click **Notebook**:

<img src="./images/New16.PNG" width="550"><br />

To create a blank notebook:

- Click **Blank**.
- On the New notebook page, add a name and optional description for the notebook.
- Specify the language as Python.
- Specify the runtime environment. 
- Click **Create**.


<img src="./images/New20.PNG" width="550"><br />

To add a notebook from your file system:

- Click **From File**.
- On the New notebook page, add a name and optional description for the notebook.
- Specify the runtime environment. 
- Click **Drag and drop files here or upload** and select the file from your local.
- Click **Create**.


<img src="./images/New21.PNG" width="550"><br />


#### Step 5: Adding a Notebook to the Project:

Create or open a notebook in your project.

**Upload data file to the Object Store:**

1. From the upper right corner click on the icon shown below:

![image](./images/icon.png)

2. Click **Drag and drop files here or upload** to upload a data file from your file system.

![images](./images/drag.png)

3. Once the file uploaded completely, you will get the option Insert to code. There are 2 options given in drop down, 
 - Pandas DataFrame
 - Credentials

4. Use both the options one by one and your cell will be added as shown below:

![images](./images/insertcode.png)

Follow the same steps to upload the other files.







