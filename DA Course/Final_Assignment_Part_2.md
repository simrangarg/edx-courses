## Peer-graded Assignment Part 2 – Analyze the Data:

**Estimated time needed:** 45 minutes

In this hands-on lab you will take the cleaned and prepared data and create pivot tables to help you analyze the data.


### Software Used in this Assignment
The instruction videos in this course use the full Excel Desktop version as this has all the available product features, but for the hands-on labs we will be using the free 'Excel for the web' version as this is available to everyone.

Although you can use the Excel Desktop software if you have access to this version, <ins>it is recommended that you use Excel for the web</ins> for the hands-on labs as the lab instructions specifically refer to this version, and there are some small differences in the interface and available features.

### Dataset Used in this Assignment
The dataset used in this lab comes from the following source: https://www.kaggle.com/kyanyoga/sample-sales-data under a **Public Domain license**. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset provided with the lab, rather than the dataset from the original source.

### Guidelines for Submission
Download and open the <a href="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DA0130EN-SkillsNetwork/edX_DA_Capstone/Data/sales_data_sample_PART2_Start.xlsx" target="_blank">**sales_data_sample_PART2_Start.xlsx**</a> file in Excel for the web.

Use the course videos from Module 4 and the Lab; Hands-on Lab 7: Using Pivot Tables to help you complete these tasks.

### Tasks to perform:
1.	**Format the data as a table:** Use the Insert, Table command to format the data as a table.
2.	**Use AutoSum to calculate values:** Use AutoSum to find the following values for column 'E' and record each of the values:
    - SUM
    - AVERAGE
    - MIN
    - MAX
    - COUNT
3.	**Create a Pivot Table:** Use the PivotTable feature to create a pivot table that displays the PRODUCTLINE field in the Rows section, and the QUANTITYORDERED in the Values section, so that the pivot table displays the total quantity count by product.
4.	**Sort the pivot table data:** Use the Sort By Value setting on the pivot table to sort it in descending order by the sum of total quantity count.
5.	**Make two more pivot tables that are the same as the one your created in task 3:** Follow the same steps you performed in Tasks 3 and 4 to create two more identical pivot tables so that you end up with 3 worksheets that contain identical pivot tables.
6.	**Analyze data in the pivot table:** Use the PivotTable Fields pane to manipulate and analyze data in the two copied pivot table as follows:
    - In pivot table 2, add the STATUS field below the PRODUCTLINE field so that the status of order shows up with their respective counts.
    - Collapse all fields except the top one – **Classic Cars**.
    - In pivot table 3, add the STATUS field above the PRODUCTLINE field so that the different status appears first, with the different product listed underneath each status with their respective counts.
    - Collapse all fields except the top one – **Cancelled**.
7.	**Save your workbook:** Use 'Save As' to save your workbook as **sales_data_sample_PART_2.XLSX**.





## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Malika Singla, Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-03-16 | 1.0 | Simran | Created the version on GitLab|


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

