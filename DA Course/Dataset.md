### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://www.kaggle.com/uciml/restaurant-data-with-consumer-ratings under a Public Domain license. 

We are using a modified subset of that dataset for the lab, so to follow the lab instructions successfully please use the dataset provided within each module, rather than the dataset from the original source.

That dataset will be used to address the questions within the course module.
