## Problem Statement

New restaurants are opening every day. However, it has become difficult for them to compete with already established restaurants. This data aims at analysing different restaurants and customers visiting them.

The project is divided into 3 parts:

<b>PART 1:  Data Loading & Cleaning</b>

The goal of this exercise is to load & understand the data and perform data cleaning to get the final cleaned data frame for further modules of the course. 

<b>PART 2: Exploratory Data Analysis</b>

The goal of this exercise is to analyze the dataset and find out interesting insights from it.

<b>PART 3: Data Analysis using SQL</b>

The goal of this exercise is to analyze the dataset using SQL.

<b>PART 4: Data Visualization</b>

The goal of this exercise is to visualize the data. In this module you will create dashboard using the Plotly library.

<b>PART 5: Model Building</b>

The goal of this exercise is to do Model Development to find the answer to the following question:
Can a predictive model be built for future prediction of the restaurant being liked or disliked by the customers?

<b>PART 6: Github Integration</b>

In this module you’ll create a GitHub account (if you don’t already have one), cretae a repository, add all the final files of each module and commit.  


Your organization has assigned you as the lead data scientist to work on the course modules. You need to work on getting answers to them in this Capstone Project by following the standard approach of data science and machine learning.


