<img src="./images/IDSNlogo.png" width="200" height="200"/>

## From Requirements to Collection

### Objective for Exercise:
- Learning about the data science methodology, and focus on the Data Requirements and the Data Collection stages.

## Lab Instructions

This course uses **Skills Network (SN) Labs**, an online virtual lab environment to help you get hands-on experience without the hassle of installing and configuring the tools. You will get access to popular open-source data science tools like **JupyterLab Notebooks**.

**<ins>How to start the hands-on session for this module:</ins>**

Click the **View resource in a new window** button below, follow the instructions in the Notebook and start learning! :)

### From Requirements to Collection (External resource)

Skills Network Labs (SN Labs) is a virtual lab environment used in this course. Your Username and email will be passed to SN Labs and will only be used for communicating important information to enhance your learning experience.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DS0103EN/DS0103EN-Review-From-Requirements-to-Collection-py-v2.0.ipynb?lti=true) 

[Download Lab Here](https://cocl.us/DS0103EN_edX_Week_2_Notebook)

## Author(s)
<h4> Joseph Santarcangelo <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

