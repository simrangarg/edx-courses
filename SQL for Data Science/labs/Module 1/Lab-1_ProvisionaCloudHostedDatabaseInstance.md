<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Provision a Cloud Hosted Database Instance

Estimated time needed: **30** minutes

### Objective for Exercise:
- Learn about how to create your database instance in Cloud.


## Hands-on LAB: Provision a Cloud Hosted Database Instance

Kindly follow the instructions in the attached file to complete this hands-on lab to create your database instance in Cloud.

[LAB 1 - v6.1 - Create Db2 Instance](./LAB1-v6_1-CreateDb2Instance.md)

## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
