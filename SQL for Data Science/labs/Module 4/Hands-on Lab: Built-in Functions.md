<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Built-in Functions

Estimated time needed: **20** minutes

### Objective for Exercise:
- Learn about the built-in functions in SQL.

## Hands-on Lab: Built-in Functions

In the previous lesson you gained an understanding of built-in database functions.

Now create and populate the PETSALE table in the database using the attached SQL script, and practice the queries in the previous lesson to solidify your understanding of built-in database functions.

[PETSALE-CREATE.sql](./PETSALE-CREATE.sql)

**Solutions:**

[PETSALE-FUNCTIONS.sql](./PETSALE-FUNCTIONS.sql)


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
