<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Execute the SQL Queries for Final Assignment

Estimated time needed: **45** minutes

### Objective for Exercise:
- Understand 3 Chicago datasets  
- Load the 3 datasets into 3 tables in a Db2 database
- Execute SQL queries to answer assignment questions

## LAB: Execute the SQL Queries for Final Assignment

#### About this Lab

In this lab your will demonstrate your working knowledge and proficiency in SQL that you have acquired in this course and utilize a Jupyter notebook to compose and execute SQL Queries for the Final Assignment. This work will be needed for the Final Assignment Assessment that follows next.

### Hands-on Lab for Final Assignement (External resource)

You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Their usage is in accordance with the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from courses.edx.org.

[Launch JupyterLab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week4-2-2-PeerAssign-v5-py.ipynb?lti=true) 

**[Optional] Download Jupyter Notebook**

In case you encounter any issues launching the Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As...":

https://ibm.box.com/shared/static/xg4pfi4hosywxbiorjjh9oejpd1jlokh.ipynb


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
