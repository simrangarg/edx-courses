<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Working with a real world data-set using SQL and Python

Estimated time needed: **45** minutes

### Objective for Exercise:
- Learn about how to Work with a real world data-set using SQL and Python.

## Hands-on Lab 1: Practice Querying Real World Data Sets (External resource)

In this hands-on lab you will work with a real-world dataset and query it using SQL from a Jupyter notebook. It will also help prepare you for the final assignment.

You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from courses.edx.org.

[Launch JupyterLab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week4-1-1-RealDataPractice-v4-py.ipynb?lti=true)

In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may download the Jupyter notebook by right clicking on the link below and choosing  "Save Link As..."

https://ibm.box.com/shared/static/0jyk14ya5fykdwkvw7hbxfe5n6jw03ed.ipynb


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
