<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Analyzing a Real World Data Set

Estimated time needed: **45** minutes

### Objective for Exercise:
- Analyzing a real world data-set with SQL and Python.


## Hands-on LAB 3: Analyzing a Real World Data Set (External resource)

In this Lab you will utilize a real world data set and analyze it using SQL queries. As in the previous Lab, you will access a Db2 on Cloud database from JupyterLab using the simplified SQL Magic interface. You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from courses.edx.org.

[Launch Jupyter Lab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week3-1-4-Analyzing-v5-py.ipynb?lti=true)


[Download the lab from here](https://ibm.box.com/shared/static/slhr5tl3gmrwzzymu2ivld35ktfc8fn1.ipynb)

## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
