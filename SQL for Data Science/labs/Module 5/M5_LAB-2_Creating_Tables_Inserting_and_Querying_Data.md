<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Creating tables, inserting and querying Data

Estimated time needed: **30** minutes

### Objective for Exercise:
- Learn about how access DB2 on Cloud using Python.


## Hands-on LAB 2: Creating tables, inserting and querying Data (External resource)

This Lab will demonstrate how to create tables in a database, insert some rows of data into the table, invoke SQL queries, and retrieve the query result set. As in the previous lab you will be using Jupyter notebook and connecting to an instance of IBM Db2 on Cloud to complete the lab. You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from courses.edx.org.

[Launch JupyterLab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week3-1-2-Querying-v4-py.ipynb?lti=true)

In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may download the Jupyter notebook by right clicking on the link below and choosing  "Save Link As..."

https://ibm.box.com/shared/static/wvtwzk66bsxtkm1e59rwei90s8r6wv74.ipynb


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
