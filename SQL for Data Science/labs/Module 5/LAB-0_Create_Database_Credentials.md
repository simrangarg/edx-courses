<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Create Database Credentials

Estimated time needed: **5** minutes

### Objective for Exercise:
- Learn about how to create your database credentials.

## LAB 0: Create Database Credentials

Follow the instructions in the attached document to create Service Credentials for your Db2 instance.

**LAB Instructions:** [Lab 0 - v6 - Create Db2 Service Credentials](./LAB-0v6_Create_Database_Credentials.md)


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
