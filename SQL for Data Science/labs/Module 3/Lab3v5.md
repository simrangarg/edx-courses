<img src="./images/IDSNlogo.png" width="200" height="200"/>

## LAB: String Patterns, Sorting & Grouping

The practice problems for this Lab will provide hands on experience with string patterns, sorting result sets and grouping result sets. You will also learn how to run SQL scripts to create several tables at once, as well as how to load data into tables from .csv files.

### HR Database

We will be working on a sample HR database for this Lab. This HR database schema consists of 5 tables called EMPLOYEES, JOB_HISTORY, JOBS, DEPARTMENTS and LOCATIONS. Each table has a few rows of sample data The following diagram shows the tables for the HR database.

![image](./images/Sample_1.PNG)

To complete this lab you will utilize Db2 database service on IBM Cloud as you did for the previous lab. There are three parts to this lab:

I.	Creating tables

II.	Loading data into tables

III.Composing and running queries

## Part I: CREATING TABLES

If you do not yet have access to Db2 on IBM Cloud, please refer to Lab Instructions in the Module/Week 1.

Rather than create each table manually by typing the DDL commands in the SQL editor, you will execute a script containing the create table commands for all the tables. Following step by step instructions are provided to perform this:

1)	Download the script file **Script_Create_Tables.sql** provided on the Lab page

2)	Login to IBM Cloud and go to the Resources Dashboard: https://cloud.ibm.com/resources where you can find the Db2 service that you created in a previous Lab. Click on the Db2-xx service. Next, open the Db2 Console by clicking on `Open Console` button. Go to the Run SQL page. The Run SQL tool enables you to run DDL and SQL statements.

3)	Click on the `+` (Add New Script) icon

![image](./images/Add_new_Script.PNG)

Click on `From File`

![image](./images/from_file.PNG)

Locate the file Script_Create_Tables.sql that you downloaded to your computer earlier and open it.

![image](./images/Sample_2.PNG)

4)	Once the statements are in the SQL Editor tool , you can run the queries against the database by selecting the `Run All` button.

![image](./images/Run_All.PNG)

5)	On the right side of the SQL editor window you will see a Result section. Clicking on a query in the Result scetion will show the execution details of the job - whether it ran successfully, or had any errors or warnings. Ensure your queries ran successfully and created all the tables.

![image](./images/Sample_3.PNG)

6)	Now you can look at the tables you created. Navigate to the three bar menu icon, select <italic>Explore</italic>, then click on <italic>Tables</italic>.

![image](./images/Sample_4.PNG)

Select the Schema corresponding to your Db2 userid. It is typically starts with 3 letters (not SQL) followed by 5 numbers (but will be different from the **QWX76809** example below). Then on the right side of the screen you should see the 5 newly created tables listed – DEPARTMENTS, EMPLOYEES, JOBS, JOB_HISTORY, and LOCATIONS (plus any other tables you may have created in previous labs e.g.
INSTRUCTOR, TEST, etc.).

![image](./images/Sample_5.PNG)

Click on any of the tables and you will see its SCHEMA definition (that is list of columns, their data types, etc).

![image](./images/Sample_6.PNG)


## Part II: LOADING DATA

Now let us see how data can be loaded into Db2. We could manually insert each row into the table one by one but that would take a long time. Instead, Db2 (and almost every other database) allows you to Load data from .CSV files.

Please follow the steps below which explains the process of loading data into the tables we created earlier.

1)	Download the 5 required data source files from the lab page in the course: **(Employees.csv,Departments.csv,Jobs.csv,JobsHistory.csv,Locations.csv)** to your computer:

2)	First let us learn how to load data into the Employees table that we created earlier. From the 3 bar menu icon, select <italic>Load</italic> then <italic>Load Data</italic>:

![image](./images/load.PNG)

On the Load page that opens ensure <italic>My Computer</italic> is selected as the source. Click on the <italic>browse files</italic> link.

![image](./images/from_computer.PNG)

3)	Choose the file `Emloyees.csv` that you downloaded to your computer and click <italic>Open</italic>.

![image](./images/select_file.PNG)

4)	Once the File is selected click <italic>Next</italic> in the bottom right corner.

![image](./images/Next_1.PNG)

5)	Select the schema for your Db2 Userid.

**NOTE:** if you only see 2-3 schemas and not your Db2 schema then scroll down in that list till you see the desired one in which you previously created the tables.

![image](./images/load_2.PNG)

It will show all the tables that have been created in this Schema previously, including the Employees table. Select the EMPLOYEES table, and choose <italic>Overwrite table with new data</italic> then click `Next`.

![image](./images/load_3.PNG)

6)	Since our source data files do not contain any rows with column labels, turn off the setting for <italic>Header in first row</italic>. Also, click on the down arrow next to <italic>Date format</italic> and choose `MM/DD/YYYY` since that is how the date is formatted in our source file.

![image](./images/load_4.PNG)

7)	Click `Next`. Review the Load setting and click `Begin Load` in the top-right corner.

![image](./images/load_5.PNG)

8)	After Loading is complete you will notice that we were successful in loading all 10 rows of the Employees table. If there are any Errors or Warnings you can view them on this screen.

![image](./images/load_6.PNG)

9)	You can see the data that was loaded by clicking on the View Table. Alternatively you can go into the Explore page and page select the correct schema, then the EMPLOYEES table, and click `View Data`.

![image](./images/load_7.PNG)

10) Now its your turn to load the remaining 4 tables of the HR database – Locations, JobHistory, Jobs, and Departments. Please follow the steps above to load the data from the remaining source files.

**Question 1:** Were there any warnings loading data into the JOBS table? What can be done to resolve this?

Hint: View the data loaded into this table and pay close attention to the JOB_TITLE column.


**Question 2:** Did all rows from the source file load successfully in the DEPARTMENT table? If not, are you able to figure out why not?

**Hint:** Look at the warning. Also, note the Primary Key for this table.

## Part III: COMPOSING AND RUNNING QUERIES

You created the tables for the HR database schema and also learned how to load data into these tables. Now try and work on a few advanced DML queries that were introduced in this module.

Follow these steps to create and run the queries indicated below

1)	Navigate to the Run SQL tool in Db2 on Cloud

2)	Compose query and run it.

3)	Check the Logs created under the Results section. Looking at the contents of the Log explains whether the SQL statement ran successfully. Also look at the query results to ensure the output is what you expected.

**Query 1: Retrieve all employees whose address is in Elgin,IL**

[Hint: Use the LIKE operator to find similar strings]

**Query 2: Retrieve all employees who were born during the 1970's.**

[Hint: Use the LIKE operator to find similar strings]

**Query 3: Retrieve all employees in department 5 whose salary is between 60000 and 70000 .**

[Hint: Use the keyword BETWEEN for this query ]

**Query 4A: Retrieve a list of employees ordered by department ID.**

[Hint: Use the ORDER BY clause for this query]

**Query 4B: Retrieve a list of employees ordered in descending order by department ID and within each department ordered alphabetically in descending order by last name.**

**Query 5A: For each department ID retrieve the number of employees in the department.**

[Hint: Use COUNT(*) to retrieve the total count of a column, and then GROUP BY]

**Query 5B: For each department retrieve the number of employees in the department, and the average employees salary in the department.**

[Hint: Use COUNT(*) to retrieve the total count of a column, and AVG() function to compute average salaries, and then group]

**Query 5C: Label the computed columns in the result set of Query 5B as `NUM_EMPLOYEES` and `AVG_SALARY`.**

[Hint: Use AS “LABEL_NAME” after the column name]

**Query 5D: In Query 5C order the result set by Average Salary.**

[Hint: Use ORDER BY after the GROUP BY]

**Query 5E: In Query 5D limit the result to departments with fewer than 4 employees.**

[Hint: Use HAVING after the GROUP BY, and use the count() function in the HAVING clause instead of the column label.]

**Note: WHERE clause is used for filtering the entire result set whereas the HAVING clause is used for filtering the result of the grouping**

**BONUS Query 6: Similar to 4B but instead of department ID use department name. Retrieve a list of employees ordered by department name, and within each department ordered alphabetically in descending order by last name.**

[Hint: Department name is in the DEPARTMENTS table. So your query will need to retrieve data from more than one table. Don’t worry if you are not able to figure this one out … we’ll cover working with multiple tables in the next lesson.]


In this lab you learned how to work with string patterns, sorting result sets and grouping result sets.

Thank you for completing this lab! 

See solutions on the following page

## Lab Solutions

Please follow these steps to get the answers to the queries:

1)	Navigate to the Run SQL page on Db2 on Cloud.

2)	Download the script file(<bold>Module4_Queries.txt</bold>) or text files(<bold>Modules4_Queries.sql</bold>). Open the file with extension .sql in the editor

3)	Run the queries. Looking at the contents of the Log explains that the SQL statement that we ran was successful. Here are the results for the queries:

Query 1: Output

![image](./images/output_1.PNG)

Query 2: Output

![image](./images/output_2.PNG)

Query 3: Output

![image](./images/output_3.PNG)

Query 4A: Output

![image](./images/output_4a.PNG)

Query 4B: Output

![image](./images/output_4b.PNG)

Query 5A: Output

![image](./images/output_5a.PNG)

Query 5B: Output

![image](./images/output_5b.PNG)

Query 5C: Output

![image](./images/output_5c.PNG)

Query 5D: Output

![image](./images/output_5d.PNG)

Query 5E: Output

![image](./images/output_5e.PNG)

BONUS Query 6: Output

Note that in the Query below `D` and `E` are aliases for the table names. Once you define an alias like `D` in your query, you can simply write `D.COLUMN_NAME` rather than the full form `DEPARTMENTS.COLUMN_NAME`.

![image](./images/output_6.PNG)


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

