<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Upload and Classify Your Images!

Estimated time needed: **30** minutes

### Objective for Exercise:

After completing this lab, you will be able to:

1. Access IBM Cloud

2. Add resources to your IBM Cloud account

3. Add services to your IBM Cloud account

4. Create a project in Watson Studio

5. Analyze images using Watson VR


### ABOUT THIS EXERCISE:

In this lesson, we'd like to take you on a bit of a side journey. It's a fun exercise and we hope you enjoy it as much as we enjoyed putting this short exercise together for you. This exercise is ungraded, so feel free to have fun with it!

### IMAGE CLASSIFICATION WITH IBM WATSON VISUAL RECOGNITION:

What better way to understand the applications of data science than to try it out yourself? You'll be uploading images and seeing how IBM Watson identifies the various objects and faces (even gender and age!) in your images.

#### Scenario

IBM Watson Visual Recognition (VR) is a service that uses deep learning algorithms to identify objects and other content in an image. In this hands-on lab, you will use Watson VR to upload and classify images. 

**Note:** To complete this exercise, you will create an IBM Cloud account and provision an instance of the Watson Visual Recognition service. A credit card is NOT required to sign up for an IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service.

### Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

If you already have an IBM Cloud account, you can skip Tasks 1 and 2 and proceed with Task 3: Login to you IBM Cloud account.

#### Task 1: Sign up for IBM Cloud

1. Go to: [Create a free account on IBM Cloud](https://cocl.us/ibm_watson_visual_recognition_ai101_coursera)

2. In the **Email** box, enter your email address and then click the arrow.

<img src="./images/DS0101EN_Lab1_Image1.png" width="550"/><br />

3. When your email address is accepted, enter your **First Name**, **Last Name**, **Country or Region**, and create a **Password**.

**Note:** To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

Then click **Create Account** to create your IBM Cloud account.

#### Task 2: Confirm your email address

1. An email is sent to the address that you signed up with.

<img src="./images/DS0101EN_Lab1_Image2.png" width="300"/><br />

2. Check your email, and in the email that was sent to you, click **Confirm Account**.

<img src="./images/DS0101EN_Lab1_Image3.png" width="550"/><br />

3. You will receive notification that your account is confirmed.

<img src="./images/DS0101EN_Lab1_Image4.png" width="300"/><br />

Click **Log In**, and you will be directed to the IBM Cloud Login Page.

#### Task 3: Login to your IBM Cloud account

1. On the [Log in to IBM Cloud](https://cloud.ibm.com/login) page, in the ID box, enter your email address and then click **Continue**.

<img src="./images/DS0101EN_Lab1_Image5.png" width="550"/><br />

2. In the **Password** box, enter your password, and then click **Log in**.

<img src="./images/DS0101EN_Lab1_Image6.png" width="550"/><br />

### Exercise 2: Create a Watson Studio Resource

#### Scenario

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

#### Task 1: Add Watson Studio as a resource

1. On the Dashboard, click **Create Resource**.

<img src="./images/DS0101EN_Lab1_Image7.png" width="550"/><br />

2. In the Catalog, click **AI (16)**.

<img src="./images/DS0101EN_Lab1_Image8.png" width="550"/><br />

Note that the **Lite** Pricing Plan is selected.

3. In the list of **Services**, click **Watson Studio**.

<img src="./images/DS0101EN_Lab1_Image9.png" width="550"/><br />

4. On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create**.

<img src="./images/DS0101EN_Lab1_Image10.png" width="550"/><br />

5. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**.

<img src="./images/DS0101EN_Lab1_Image11.png" width="550"/><br />

6. You will see this message when Watson Studio is successfully set up for you.

<img src="./images/DS0101EN_Lab1_Image12.png" width="300"/><br />

Click **Get Started**.

### Exercise 3: Create a project

#### Scenario

To manage all the resources and services that you are working with, you should create a Watson Studio Project. You will begin by creating an empty project, and then adding the resources and services that you need. 

#### Task 1: Create an empty project

1.On the Watson Studio Welcome page, click **Create a project**.

<img src="./images/DS0101EN_Lab1_Image13.png" width="550"/><br />

2. On the Create a project page, click **Create an empty project**.

<img src="./images/DS0101EN_Lab1_Image14.png" width="550"/><br />

3. On the New project page, enter a **Name** and **Description** for your project.

<img src="./images/DS0101EN_Lab1_Image15.png" width="550"/><br />

4. You must define storage for your project before you can create it. Under **Select storage service**, click **Add**.

<img src="./images/DS0101EN_Lab1_Image16.png" width="550"/><br />

5. On the Cloud Object Storage page, verify that **Lite** is selected, and then click **Create**.

<img src="./images/DS0101EN_Lab1_Image17.png" width="550"/><br />

6. In the Confirm Creation box, click **Confirm**.

<img src="./images/DS0101EN_Lab1_Image18.png" width="250"/><br />

7. On the New project page, under **Define storage**, click **Refresh**, and then click **Create**.

### Exercise 4: Add a Watson VR Service instance

#### Scenario

This project will focus on analyzing images, so you need to add the Watson Visual Recognition Service. You will also need some images to analyze, so follow the setup steps below to ensure you are prepared.

#### Setup 

Before you begin this exercise, you must complete the following steps:

1. Collect a set of at least 20 images. You can use your own images, or download them from the internet. 

2. Store the images in an easy to find location.

#### Task 1: Add the Visual Recognition Service

1. To add services to the project, click **Add to project**.

<img src="./images/DS0101EN_Lab1_Image19.png" width="550"/><br />

2. In the Choose asset type box, click **Visual Recognition**.

<img src="./images/DS0101EN_Lab1_Image20.png" width="550"/><br />

3. In the Associate a service box, click **here**.

<img src="./images/DS0101EN_Lab1_Image21.png" width="550"/><br />

4. On the Visual Recognition page, verify that **Lite** is selected, and then click **Create**.

<img src="./images/DS0101EN_Lab1_Image22.png" width="550"/><br />

5. In the Confirm Creation box, click **Confirm**.

<img src="./images/DS0101EN_Lab1_Image23.png" width="250"/><br />

#### Task 2: Analyze images with Watson VR

1. Now you can see all the built-in image classification models that IBM Watson provides! Let's try the General model.

To analyze your images, on the Models page, under **Prebuilt Models**, in the **General** box, click **Test**.

<img src="./images/DS0101EN_Lab1_Image24.png" width="550"/><br />

2. On the General page, click the **Test** tab.

<img src="./images/DS0101EN_Lab1_Image25.png" width="550"/><br />

3. To upload images, on the Test tab, click **Browse**.

<img src="./images/DS0101EN_Lab1_Image26.png" width="500"/><br />

4. Select the images you want to upload and then click **Open**.

<img src="./images/DS0101EN_Lab1_Image27.png" width="550"/><br />

5. Once you have uploaded your images, Watson Studio Visual Recognition will tell you what it thinks it found in your images! Beside each class of object (or color, age, etc.), it gives you a confidence score (between 0 and 1) showing how confident it is that it found that particular object or feature in your image (0 for lowest confidence and 1 for highest confidence).

<img src="./images/DS0101EN_Lab1_Image28.png" width="550"/><br />

6. Use the check boxes on the left to filter the images. In this example, only images in which Watson VR has detected **beige color** are displayed.

<img src="./images/DS0101EN_Lab1_Image29.png" width="550"/><br />

7. Use the **Threshold** slider to only display images in which Watson VR has at least 90% confidence of the beige color.

<img src="./images/DS0101EN_Lab1_Image30.png" width="550"/><br />

#### Task 3: Share your results

Follow us on Twitter and send us some of the funniest and most interesting results you found with IBM Watson Visual Recognition!

<img src="./images/DS0101EN_Lab1_Image31.png" width="300"/><br />

[Follow Polong Lin](https://twitter.com/polonglin)

[Follow Alex Aklson](https://twitter.com/aklson_DS)


## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

