<img src="./images/IDSNlogo.png" width="200" height="200"/>

<h3>Create a New Studio Project</h3>
<ol>
<li>
<p>Log in to <a href="https://dataplatform.cloud.ibm.com/?cm_mmc=Email_Newsletter-_-Developer_Ed+Tech-_-WW_WW-_edx-Cognitive-Class-DS0720EN-Watson-Studio-login-&amp;cm_mmca1=000026UJ&amp;cm_mmca2=10006555&amp;cm_mmca3=M12345678&amp;cvosrc=email.Newsletter.M12345678&amp;cvo_campaign=000026UJ" target="_blank">Watson Studio</a> and click <strong>Create a project</strong>.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/74567c519ba8da39de96416ddd9b8fa6/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_new_project1.png" alt="" width="585" height="398" /></p>
</li>
<li>
<p>On the <strong>Data Science</strong> tile, click <strong>Create Project</strong> or create a Data Science project from your My Projects page.</p>
<p>For information about the different types of projects, see <a href="https://dataplatform.cloud.ibm.com/docs/content/manage-data/project-tools.html?audience=wdp&amp;context=wdp&amp;linkInPage=true" target="_blank">Project starters</a>.&nbsp;&nbsp;</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/add4a36bb05beb542baad44bf8ca2c8f/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_new_project2.png" alt="" width="711" height="265" /></p>
</li>
<li>
<p>On the New project page, add a name and optional description for your project. Choose whether to restrict who you can add as a collaborator. You cannot change this setting after you create the project. When you're done, click <strong>Create</strong>.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/609796e9f2726e3bfdc65c0dae95a5b8/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_new_project3.png" alt="" width="654" height="725" /></p>
Your new project opens and you can start adding resources to it.</li>
</ol>


```

```

## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

