<img src="./images/IDSNlogo.png" width="200" height="200"/>

<h3>Add Files to Your Project</h3>
<p>An instance of the Cloud Object Storage service is automatically created when your project was created.</p>
<ol>
<li>
<p>Create or open a notebook in your project.</p>
</li>
<li>
<p>Upload any data file to the Object Store:</p>
<ul>
<li>
<p>From the upper right corner, click the <strong>1001</strong> icon.</p>
</li>
<li>
<p>Click <strong>Browse</strong> to upload a data file from your file system.</p>
</li>
</ul>
</li>
<li>
<p>Create a Panda Dataframe out of the uploaded file by going to the uploaded file drop-down menu and selecting <strong>Insert pandas DataFrame</strong>. All the required Object Store credentials are collected and inserted into the code.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/f5e6f5e5059d1ecba130aa3dc0bc1b59/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_add_file_project.png" alt="" width="913" height="630" /></p>
</li>
</ol>


## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

