<img src="./images/IDSNlogo.png" width="200" height="200"/>

### Ingest the NYC PLUTO Dataset

1. Use the PLUTO dataset URL to download the zip file: https://www1.nyc.gov/assets/planning/download/zip/data-maps/open-data/nyc_pluto_18v1.zip.

2. Extract the file. You should have five CSV files for the five New York city boroughs: Bronx, Brooklyn, Manhattan, Queens, and Staten Island.

3. Read each CSV file and upload them to the IBM Cloud Object Store by using your credentials.


## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

