<img src="./images/IDSNlogo.png" width="200" height="200"/>

## What Is the Relationship between Housing Characteristics and Complaints?

Estimated time needed: **3** hours

### Objective for Exercise:

- Use your data visualization skills to visualize the data and extract meaningful patterns to guide the modelling process.

The goal of this exercise is to find the answer to the Question 3 of the problem statement: 

Does the Complaint Type that you identified in response to Question 1 have an obvious relationship with any particular characteristic or characteristic of the Houses?

In this exercise, use the 311 dataset.

You also need to read back the PLUTO dataset from Cloud Object Store that you saved previously in the course. Use the PLUTO dataset for the borough that you already identified to focus on the last exercise.Ensure that you use only a limited number of fields from the dataset so that you are not consuming too much memory during your analysis.

The recommended fields are Address, BldgArea, BldgDepth, BuiltFAR, CommFAR, FacilFAR, Lot, LotArea, LotDepth, NumBldgs, NumFloors, OfficeArea, ResArea, ResidFAR, RetailArea, YearBuilt, YearAlter1, ZipCode, YCoord, and XCoord.

At the end of this exercise, you should determine whether the type of complaint that you have identified as the response to Question 1 has an obvious relationship with any particular characteristic or characteristics of the houses.

Add your answer to this question along with code and comments in a separate notebook. Upload the notebook in the subsection called "Question 3 - Does the Complaint Type, that you identified in response to Question 1, have an obvious relationship with any particular characteristic(s) of the Houses?" in the section "Submit your work and Review your Peer's work" in the module "Submit Your Work and Grade Your Peers".


## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

