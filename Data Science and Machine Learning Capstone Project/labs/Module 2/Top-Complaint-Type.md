<img src="./images/IDSNlogo.png" width="200" height="200"/>

## What Is the Top Complaint Type?

Estimated time needed: **2** hours

### Objective for Exercise:

- Use data science methodologies to define and formulate a real-world business problem.

The goal of this exercise is to find the answer to the Question 1 of the problem statement:

**Which type of complaint should the Department of Housing Preservation and Development of New York City focus on first?**

In this exercise, you need to read back the 311 datasets that you stored in Cloud Object Store and explore the dataset.

By the end of this exercise, you need to figure out the correct Complaint Type that the Department of Housing Preservation and Development of New York City should focus on.

Add the answer to this question along with code and comments in a notebook. Upload the notebook in the subsection called "Question 1 - Which type of complaints The Department of Housing Preservation and Development of New York City should focus first?"  in the section "Submit your work and Review your Peer's work' in module 'Submit Your Work and Grade Your Peers".

## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

