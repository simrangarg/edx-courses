<img src="./images/IDSNlogo.png" width="200" height="200"/>

## What Areas Should the Agency Focus On?

Estimated time needed: **2** hours

### Objective for Exercise:
- Use your data analysis tools to ingest a dataset, clean it, and wrangle it..

The goal of this exercise is to do explore the data to find the answer to the Question 2 problem statement:

**Should the Department of Housing Preservation and Development of New York City focus on any particular set of boroughs, ZIP codes, or street (where the complaints are severe) for the specific type of complaints you identified in response to Question 1?**

In this exercise, you will use 311 Dataset to determine whether to focus on any particular borough, ZIP code, or street (where the complaints are severe) for the specific Complaint Type you decided to focus at the end of the last exercise.

Add your answer to this question along with code and comments in a separate notebook. Upload the notebook in the subsection called "Question 2 - Should the Department of Housing Preservation and Development of New York City focus on any particular set of Boroughs or ZIP Code or Streets (where the complaints are severe) for the specific type of Complaints you identified in response to Question 1"  in the section "Submit your work and Review your Peer's work" in the module "Submit Your Work and Grade Your Peers".

## Author(s)
<h4> Nayef Abou Tayoun <h4/>

### Other Contributor(s) 
Himanshu Birla, Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

