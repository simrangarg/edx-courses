<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>

### Lab 3: Exploratory Data Analysis (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* In this section, we will explore several methods to see if certain characteristics or features can be used to predict price.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/exploratory-data-analysis.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Exploratory-Data-Analysis.ipynb)




## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
