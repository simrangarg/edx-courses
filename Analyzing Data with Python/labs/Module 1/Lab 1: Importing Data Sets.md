<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>

### Lab 1: Importing Data Sets (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* In this lab, you will learn the basics of loading and viewing data with Pandas


[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/review-introduction.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Introduction.ipynb)















## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>