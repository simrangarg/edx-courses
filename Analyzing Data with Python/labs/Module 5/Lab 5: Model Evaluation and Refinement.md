<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>

### Lab: Model Evaluation (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* We have built models and made predictions of vehicle prices. Now we will determine how accurate these predictions are.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/model-evaluation-and-refinement.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Model-Evaluation-and-Refinement.ipynb)




## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

