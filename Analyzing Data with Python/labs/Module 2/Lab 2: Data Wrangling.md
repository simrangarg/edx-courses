<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>

### Lab: Data Wrangling (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* By the end of this notebook, you will have learned the basics of Data Wrangling

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/data-wrangling.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Data-Wrangling.ipynb)





## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |
