<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>

Let's learn about KNN and see how we can apply it real world problems.


**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: KNN (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* In this Lab you will load a customer dataset related to a telecommunication company, clean it, use KNN (K-Nearest Neighbours to predict the category of customers, and evaluate the accuracy of your model.

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Clas-K-Nearest-neighbors-CustCat-py-v1.ipynb?lti=true) 

You can download the lab [HERE](https://cocl.us/ML0101EN-Clas-K-Nearest-neighbors-CustCat-py-v1.ipynb)


## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
