<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Final Project Setup

*If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:*

#### Step 1: For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

[Click here](https://cocl.us/cc_python101_pandas_Watson)

You will see the screen in the figure below. Click the icon in the red box:

<img src="./images/DA0101EN_FA_Image1.png" width="550"><br />

Then click **Watson**, as shown below:

<img src="./images/DA0101EN_FA_Image2.png" width="550"><br />

Then click **Browse Services**.

<img src="./images/DA0101EN_FA_Image3.png" width="550"><br />

Scroll down and select **Watson Studio - Lite**.

<img src="./images/DA0101EN_FA_Image4.png" width="550"><br />

To create a Watson service using the Lite plan, click **Create**.

<img src="./images/DA0101EN_FA_Image5.png" width="550"><br />

Now click **Get Started**.

<img src="./images/DA0101EN_FA_Image6.png" width="550"><br />

After creating the service continue with Step 2.

#### Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services**.

<img src="./images/DA0101EN_FA_Image7.png" width="550"><br />

When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

<img src="./images/DA0101EN_FA_Image8.png" width="550"><br />

Then click **Get Started**.

<img src="./images/DA0101EN_FA_Image9.png" width="550"><br />

#### Step 3: Creating a Project

Now you have to Create a project.

Click on **Create a project**:

<img src="./images/DA0101EN_FA_Image10.png" width="550"><br />

On the Create a project page, click **Create an empty project**

<img src="./images/DA0101EN_FA_Image11.png" width="550"><br />

Provide a **Project Name** and **Description**, as shown below:

<img src="./images/DA0101EN_FA_Image12.png" width="550"><br />

You must also create storage for the project.

Click **Add**

<img src="./images/DA0101EN_FA_Image13.png" width="550"><br />

On the Cloud Object Storage page, scroll down and then click **Create**.

<img src="./images/DA0101EN_FA_Image14.png" width="550"><br />

In the Confirm Creation box, click **Confirm**.

<img src="./images/DA0101EN_FA_Image15.png" width="250"><br />

On the New project page, note that the storage has been added, and then click **Create**.

<img src="./images/DA0101EN_FA_Image16.png" width="550"><br />

After creating the project continue with Step 3.

#### Step 3: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project**.

<img src="./images/DA0101EN_FA_Image17.png" width="550"><br />

In the list of asset types, click **Notebook**:

<img src="./images/DA0101EN_FA_Image18.png" width="550"><br />

On the New Notebook page, enter a name for the notebook, and then click **From URL**.

Copy this link:

[Click here](https://cocl.us/ML0101EXpog)

Paste it into the **Notebook URL** box, and then click **Create Notebook**.

<img src="./images/DA0101EN_FA_Image19.png" width="550"><br />

You will see this Notebook:

<img src="./images/DA0101EN_FA_Image20.png" width="550"><br />

Once you complete your notebook you will have to share it. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all content excluding sensitive code cells.

<img src="./images/ML0101EN_Image1.png" width="550"><br />

<img src="./images/ML0101EN_Image2.png" width="550"><br />

<img src="./images/ML0101EN_Image3.png" width="550"><br />


## Author(s)
<h4> Joseph Santarcangelo <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
