<img src="./images/IDSNlogo.png" width="200" height="200"/>

## DBSCAN Clustering

Estimated time needed: **1** Hour

### Objective for Exercise:
- Learn how to manipulate the data and properties of DBSCAN and observing the resulting clustering.

Density-based Clustering locates regions of high density that are separated from one another by regions of low density. Density, in this context, is defined as the number of points within a specified radius.

In this section, the main focus will be manipulating the data and properties of DBSCAN and observing the resulting clustering.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**


### Lab: DBSCAN Clustering (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/ML0101EN/ML0101EN-Clus-DBSCN-weather-py-v1.ipynb?lti=true)

You can download the lab [HERE](https://cocl.us/ML0101EN-Clus-DBSCN-weather-py-v1.ipynb)


## Author(s)
<h4> Joseph Santarcangelo <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
