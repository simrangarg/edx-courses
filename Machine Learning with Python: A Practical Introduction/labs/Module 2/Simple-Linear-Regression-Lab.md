<center>
    <img src="./images/IDSNlogo.png" width="200" height="200"/>
</center>



**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

This course uses a third-party tool, Skills Network (SN) Labs, to enhance your learning experience. The tool will reference basic information like your name and email address.

### Lab: Simple Linear Regression (External resource)

Estimated time needed: **10** minutes

Objective for Exercise:

* In this lab, we learn how to use scikit-learn library to implement Simple linear regression. We download a data set that is related to fuel consumption and Carbon dioxide emission of cars. Then, we split our data into training and test sets, create a model using training set, evaluate your model using test set, and finally use model to predict unknown value. Let's get started


Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Reg-Simple-Linear-Regression-Co2-py-v1.ipynb?lti=true) 

You can download the lab [HERE](https://cocl.us/ML0101EN-Reg-Simple-Linear-Regression-Co2-py-v1.ipynb)


## Author(s)

<a href="https://www.linkedin.com/in/joseph-s-50398b136/" target="_blank">Joseph Santarcangelo</a>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Parvez | Initial version created |
|   |   |   |   |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>