<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Collaborative Filtering on Movies

Estimated time needed: **1** Hour

### Objective for Exercise:
- We will explore recommendation systems based on Collaborative Filtering and implement simple version of one using Python and the Pandas library.


Recommendation systems are a collection of algorithms used to recommend items to users based on information taken from the user. These systems have become ubiquitous can be commonly seen in online stores, movies databases and job finders. In this notebook, we will explore recommendation systems based on Collaborative Filtering and implement simple version of one using Python and the Pandas library.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: Collaborative Filtering on Movies (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/ML0101EN/ML0101EN-RecSys-Collaborative-Filtering-movies-py-v1.ipynb?lti=true)

You can download the lab [HERE](https://cocl.us/ML0101EN-RecSys-Collaborative-Filtering-movies-py-v1.ipynb)


## Author(s)
<h4> Joseph Santarcangelo <h4/>

### Other Contributor(s) 
Simran Garg

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 0.1 | Simran | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
